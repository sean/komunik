defmodule Komunik.BoardTest do
  use Komunik.DataCase
  alias Komunik.{Ranking, Board}

  test "can create categories without giving a rank" do
    {:ok, category} =
      %{name: "Bob"}
      |> Board.create_board_category()

    assert category.sort_rank != nil
    assert category.sort_rank == Ranking.new()
  end

  test "can create categories and give a rank to create before" do
    {:ok, category} =
      %{name: "Bob"}
      |> Board.create_board_category(:start, "Uzzzzzzz")

    assert category.sort_rank < "Uzzzzzzz"
  end

  test "can create categories and give a rank to create after" do
    {:ok, category} = %{name: "Bob"} |> Board.create_board_category(:end, "Uzzzzzzz")

    assert category.sort_rank > "Uzzzzzzz"
  end

  test "can move a category before an existing one" do
    category_to_move = insert(:board_category)

    category_to_move_before =
      insert(:board_category, sort_rank: Ranking.prev(category_to_move.sort_rank))

    assert category_to_move.sort_rank > category_to_move_before.sort_rank

    {:ok, actual} =
      category_to_move |> Board.move_category_start(category_to_move_before.sort_rank)

    assert actual.sort_rank < category_to_move_before.sort_rank
  end

  test "can move a category after an existing one" do
    category_to_move = insert(:board_category)

    category_to_move_after =
      insert(:board_category,
        sort_rank: Ranking.next(category_to_move.sort_rank)
      )

    assert category_to_move.sort_rank < category_to_move_after.sort_rank

    {:ok, actual} = category_to_move |> Board.move_category_end(category_to_move_after.sort_rank)

    assert actual.sort_rank > category_to_move_after.sort_rank
  end

  test "can move a category between two categories" do
    category_to_move = insert(:board_category)

    category_to_move_after =
      insert(:board_category, sort_rank: category_to_move.sort_rank |> Ranking.next())

    category_to_move_before =
      insert(:board_category, sort_rank: category_to_move_after.sort_rank |> Ranking.next())

    assert category_to_move.sort_rank < category_to_move_after.sort_rank
    assert category_to_move.sort_rank < category_to_move_before.sort_rank

    {:ok, actual} =
      category_to_move
      |> Board.move_category_between(
        category_to_move_after.sort_rank,
        category_to_move_before.sort_rank
      )

    assert actual.sort_rank > category_to_move_after.sort_rank
    assert actual.sort_rank < category_to_move_before.sort_rank
  end

  test "can move a category and then move it back" do
    category_to_move = insert(:board_category)

    category_to_move_against =
      insert(:board_category,
        sort_rank: category_to_move.sort_rank |> Ranking.next()
      )

    assert category_to_move.sort_rank < category_to_move_against.sort_rank

    {:ok, moved_category} =
      category_to_move |> Board.move_category_end(category_to_move_against.sort_rank)

    assert moved_category.sort_rank > category_to_move_against.sort_rank

    {:ok, actual} =
      moved_category |> Board.move_category_start(category_to_move_against.sort_rank)

    assert actual.sort_rank < category_to_move_against.sort_rank
  end

  test "can move a category before a category and then move it back after another category moved in the firsts old spot" do
    category_to_move = insert(:board_category)

    category_to_move_against =
      insert(:board_category,
        sort_rank: Ranking.next(category_to_move.sort_rank)
      )

    assert category_to_move.sort_rank < category_to_move_against.sort_rank

    {:ok, moved_category} =
      category_to_move |> Board.move_category_end(category_to_move_against.sort_rank)

    new_category =
      insert(:board_category, sort_rank: Ranking.prev(category_to_move_against.sort_rank))

    assert moved_category.sort_rank > category_to_move_against.sort_rank

    {:ok, actual} = moved_category |> Board.move_category_start(new_category.sort_rank)

    assert actual.sort_rank < category_to_move_against.sort_rank
  end

  test "can move a category after a category and then move it back after another category moved in the firsts old spot" do
    category_to_move = insert(:board_category)

    category_to_move_against =
      insert(:board_category, sort_rank: category_to_move.sort_rank |> Ranking.prev())

    assert category_to_move.sort_rank > category_to_move_against.sort_rank

    {:ok, moved_category} =
      category_to_move |> Board.move_category_start(category_to_move_against.sort_rank)

    new_category =
      insert(:board_category,
        sort_rank: category_to_move.sort_rank |> Ranking.next()
      )

    assert moved_category.sort_rank < category_to_move_against.sort_rank

    {:ok, actual} =
      moved_category
      |> Board.move_category_between(category_to_move_against.sort_rank, new_category.sort_rank)

    assert actual.sort_rank > category_to_move_against.sort_rank
  end

  test "can move a category two ahead" do
    category_to_move = insert(:board_category)

    {:ok, first} = Board.create_board_category(%{name: "First"}, :end, category_to_move.sort_rank)

    {:ok, second} = Board.create_board_category(%{name: "Second"}, :end, first.sort_rank)

    assert category_to_move.sort_rank < first.sort_rank
    assert category_to_move.sort_rank < second.sort_rank

    {:ok, actual} = Board.move_category_end(category_to_move, second.sort_rank)

    assert actual.sort_rank > first.sort_rank
    assert actual.sort_rank > second.sort_rank
  end

  test "can move a category two behind" do
    category_to_move = insert(:board_category)

    {:ok, first} =
      Board.create_board_category(%{name: "First"}, :start, category_to_move.sort_rank)

    {:ok, second} = Board.create_board_category(%{name: "Second"}, :start, first.sort_rank)

    assert category_to_move.sort_rank > first.sort_rank
    assert category_to_move.sort_rank > second.sort_rank

    {:ok, actual} = Board.move_category_start(category_to_move, second.sort_rank)

    assert actual.sort_rank < first.sort_rank
    assert actual.sort_rank < second.sort_rank
  end

  test "can create a category between two existing categories" do
    {:ok, category_a} = Board.create_board_category(%{name: "category_a"})

    {:ok, category_b} =
      Board.create_board_category(%{name: "category_b"}, :end, category_a.sort_rank)

    {:ok, category_c} =
      Board.create_board_category(
        %{name: "category_c"},
        :between,
        category_a.sort_rank,
        category_b.sort_rank
      )

    assert category_c.sort_rank > category_a.sort_rank
    assert category_c.sort_rank < category_b.sort_rank
  end

  test "can get categories in the proper sorting with rank" do
    {:ok, category_a} = Board.create_board_category(%{name: "category_a"})

    {:ok, category_b} =
      Board.create_board_category(%{name: "category_b"}, :end, category_a.sort_rank)

    {:ok, category_c} =
      Board.create_board_category(%{name: "category_c"}, :end, category_b.sort_rank)

    {:ok, category_d} =
      Board.create_board_category(%{name: "category_d"}, :end, category_c.sort_rank)

    {:ok, category_e} =
      Board.create_board_category(%{name: "category_e"}, :end, category_d.sort_rank)

    {:ok, category_f} =
      Board.create_board_category(%{name: "category_f"}, :end, category_e.sort_rank)

    {:ok, new_category_f} =
      Board.move_category_between(category_f, category_c.sort_rank, category_d.sort_rank)

    {:ok, new_category_a} = Board.move_category_end(category_a, category_e.sort_rank)

    assert [^category_b, ^category_c, ^new_category_f, ^category_d, ^category_e, ^new_category_a] =
             Board.get_sorted_categories()
  end
end
