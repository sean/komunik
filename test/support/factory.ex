defmodule Komunik.Factory do
  use ExMachina.Ecto, repo: Komunik.Repo
  alias Komunik.Ranking
  alias Komunik.Board.Category

  def board_category_factory do
    %Category{
      name: sequence(:name, &"Name-#{&1}"),
      sort_rank: Ranking.new()
    }
  end
end
