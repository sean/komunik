@default:
  mix phx.server

test:
  mix test.watch

migrate:
  mix ecto.migrate
  MIX_ENV=test mix ecto.migrate

update-deps:
  mix do deps.get, deps.compile