defmodule Komunik.Ranking do
  def divisor(), do: 16

  def step(), do: 8

  def new() do
    LexorankEx.middle(divisor())
  end

  def next(rank) do
    LexorankEx.next(rank, step())
  end

  def prev(rank) do
    LexorankEx.prev(rank, step())
  end

  def between(rank_a, rank_b) do
    LexorankEx.between(rank_a, rank_b)
  end
end
