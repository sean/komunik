defmodule Komunik.Board do
  import Ecto.Query
  alias Komunik.{Repo, Ranking}
  alias Komunik.Board.Category

  def create_board_category(params) do
    initialize_board_category_with_rank(nil)
    |> Category.changeset(params)
    |> Repo.insert()
  end

  def create_board_category(params, position, rank) do
    initialize_board_category_with_rank(position, rank)
    |> insert_category(params)
  end

  def create_board_category(params, :between, rank_a, rank_b) do
    initialize_board_category_with_rank(:between, rank_a, rank_b)
    |> insert_category(params)
  end

  def move_category_start(category, previous_start_rank) do
    category
    |> Category.changeset(%{
      sort_rank: previous_start_rank |> Ranking.prev()
    })
    |> Repo.update()
  end

  def move_category_end(category, previous_end_rank) do
    category
    |> Category.changeset(%{
      sort_rank: previous_end_rank |> Ranking.next()
    })
    |> Repo.update()
  end

  def move_category_between(category, after_rank, before_rank) do
    category
    |> Category.changeset(%{sort_rank: Ranking.between(after_rank, before_rank)})
    |> Repo.update()
  end

  def get_sorted_categories() do
    query = from c in Category, order_by: c.sort_rank, select: c

    Repo.all(query)
  end

  defp insert_category(category, params) do
    category
    |> Category.changeset(params)
    |> Repo.insert()
  end

  defp initialize_board_category_with_rank(nil) do
    %Category{sort_rank: Ranking.new()}
  end

  defp initialize_board_category_with_rank(:start, previous_start) do
    %Category{sort_rank: previous_start |> Ranking.prev()}
  end

  defp initialize_board_category_with_rank(:end, previous_end) do
    %Category{sort_rank: previous_end |> Ranking.next()}
  end

  defp initialize_board_category_with_rank(:between, rank_a, rank_b) do
    %Category{sort_rank: Ranking.between(rank_a, rank_b)}
  end
end
