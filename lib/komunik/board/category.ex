defmodule Komunik.Board.Category do
  use Komunik.Schema

  schema "categories" do
    field :name, :string
    field :sort_rank, :string

    timestamps()
  end

  def new_changeset(category) do
    category
    |> changeset(%{})
  end

  def changeset(category, params) do
    category
    |> cast(params, [:name, :sort_rank])
    |> validate_required([:name, :sort_rank])
    |> unique_constraint(:name)
    |> unique_constraint(:sort_rank)
    |> validate_required([:name, :sort_rank])
  end
end
