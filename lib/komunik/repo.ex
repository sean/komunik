defmodule Komunik.Repo do
  use Ecto.Repo,
    otp_app: :komunik,
    adapter: Ecto.Adapters.SQLite3
end
