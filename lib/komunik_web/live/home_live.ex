defmodule KomunikWeb.HomeLive do
  use KomunikWeb, :live_view

  def render(assigns) do
    ~F"""
    <h1 class="text-3xl">Home</h1>
    """
  end
end
