defmodule KomunikWeb.BoardCategoriesLive do
  use KomunikWeb, :live_view
  alias Komunik.{Board, Board.Category}
  alias Surface.Components.{Form, Form.Field, Form.TextInput}

  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(
        categories: Komunik.Board.get_sorted_categories(),
        new_category: new_category_assign(socket.assigns.live_action)
      )

    {:ok, socket}
  end

  def handle_event("new-category", _event, socket) do
    {:noreply,
     socket
     |> push_navigate(to: Routes.board_categories_path(socket, :new))}
  end

  def handle_event("cancel-new-category", _event, socket) do
    {:noreply,
     socket
     |> push_navigate(to: Routes.board_categories_path(socket, :index))}
  end

  def handle_event("submit", data, %{assigns: %{categories: []}} = socket) do
    %{"category" => params} = data
    {:ok, _category} = params |> Board.create_board_category()

    {:noreply, push_navigate(socket, to: Routes.board_categories_path(socket, :index))}
  end

  def handle_event("submit", data, %{assigns: %{categories: categories}} = socket) do
    %{"category" => params} = data
    prev_start_category = hd(categories)

    {:ok, _category} =
      params |> Board.create_board_category(:start, prev_start_category.sort_rank)

    {:noreply, push_navigate(socket, to: Routes.board_categories_path(socket, :index))}
  end

  def render(assigns) do
    ~F"""
    <div phx-mounted={handle_mount(@live_action)} class="h-full w-full flex flex-col gap-4">
      <h1 class="text-3xl">Board Categories</h1>
      {#if !Enum.empty?(@categories)}
        <div class="flex flex-col gap-4">
          {#if @live_action == :new}
            <div class="flex flex-col gap-4">
              <Form id="new-category-form" for={@new_category} submit="submit" opts={autocomplete: "off"}>
                <Field name={:name}>
                  <TextInput
                    opts={placeholder: "New category name"}
                    class="w-full bg-gray-50 shadow-md rounded p-2"
                  />
                </Field>
              </Form>
              <button
                :on-click="cancel-new-category"
                class="bg-red-900 bg-opacity-25 hover:bg-opacity-50 p-2 text-red-900 font-bold rounded"
              >Cancel</button>
            </div>
          {#else}
            <button
              aria-label="Create new category"
              :on-click="new-category"
              class="bg-blue-900 bg-opacity-25 hover:bg-opacity-50 p-2 text-blue-900 font-bold rounded"
            >+</button>
          {/if}
          <ol id="category-list" class="flex flex-col gap-4">
            {#for {category, _i} <- Enum.with_index(@categories)}
              <li class="bg-gray-50 shadow-md rounded p-2">
                <div>Category name: {category.name}</div>
              </li>
            {/for}
          </ol>
        </div>
      {#else}
        <div class="h-full w-full grid place-items-center">
          <div class="flex flex-col text-center gap-4">
            {#if @live_action == :new}
              <div class="flex flex-col gap-4">
                <Form id="new-category-form" for={@new_category} submit="submit" opts={autocomplete: "off"}>
                  <Field name={:name}>
                    <TextInput
                      opts={placeholder: "New category name"}
                      class="w-full bg-gray-50 shadow-md rounded p-2"
                    />
                  </Field>
                </Form>
                <button
                  :on-click="cancel-new-category"
                  class="bg-red-900 bg-opacity-25 hover:bg-opacity-50 p-2 text-red-900 font-bold rounded"
                >Cancel</button>
              </div>
            {#else}
              <button
                aria-label="Create new category"
                :on-click="new-category"
                class="bg-blue-900 bg-opacity-25 hover:bg-opacity-50 p-2 text-blue-900 font-bold rounded"
              >+</button>
            {/if}
            <p class="text-gray-600">No board categories</p>
          </div>
        </div>
      {/if}
    </div>
    """
  end

  defp handle_mount(:new), do: JS.focus_first(to: "#new-category-form")
  defp handle_mount(_), do: JS.focus(to: "#category-list")

  defp new_category_assign(:new), do: Category.new_changeset(%Category{})
  defp new_category_assign(_), do: nil
end
