defmodule KomunikWeb.Router do
  use KomunikWeb, :router
  import Surface.Catalogue.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {KomunikWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", KomunikWeb do
    pipe_through :browser

    live "/", HomeLive
    live "/categories", BoardCategoriesLive, :index
    live "/categories/new", BoardCategoriesLive, :new
  end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
      surface_catalogue("/catalogue")
    end
  end
end
