defmodule KomunikWeb.Components.GlobalHeader do
  use KomunikWeb, :component
  alias Surface.Components.{LiveRedirect}

  def render(assigns) do
    ~F"""
    <header class="p-4 bg-gray-50 shadow">
      <nav class="flex gap-4">
        <LiveRedirect to="/">Home</LiveRedirect>
        <LiveRedirect to="/categories">Board Categories</LiveRedirect>
      </nav>
    </header>
    """
  end
end
