defmodule Komunik.Repo.Migrations.CreateBoardCategories do
  use Ecto.Migration

  def change do
    create table("categories") do
      add :name, :string, null: false
      add :sort_rank, :string, null: false

      timestamps()
    end

    create index("categories", [:name], unique: true)
    create index("categories", [:sort_rank], unique: true)
  end
end
